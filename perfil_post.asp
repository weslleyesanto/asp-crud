<!--#include file="include\config.asp"-->
<!--#include file="include\md5.asp"-->
<%
url = "default.asp"

if  Request.ServerVariables("REQUEST_METHOD") = "POST" then
    
    Response.ContentType = "application/json"
    
    'Instancia o componente
    SET SaFileUp = Server.CreateObject("SoftArtisans.FileUp")

    'PEGANDO CAMPOS PASSADOS DO FORMULÁRIO PARA AS VARIÁVEIS'
	foto = LimparTexto(SaFileUp.Form("foto"))
	ds_foto = LimparTexto(SaFileUp.Form("ds_foto"))
	nm_perfil = LimparTexto(SaFileUp.Form("nm_perfil"))
    dt_nascimento = LimparTexto(SaFileUp.Form("dt_nascimento"))
    id_perfil = LimparTexto(SaFileUp.Form("id_perfil"))
	id_categoria = LimparTexto(SaFileUp.Form("id_categoria"))
    modulo = "Perfil"

    if IsDate(dt_nascimento) = false then
	    retorno = "{""res"":""error"", ""msg"":""Data inválida!""}"
	    Response.write(retorno)
    	response.end
    else
    	dt_nascimento = FormataData(dt_nascimento, 1)
    end if

    if validaEmail(SaFileUp.Form("email")) = false then
    	retorno = "{""res"":""error"", ""msg"":""Preencha o E-mail corretamente!""}"
	    Response.write(retorno)
    	response.end
    else
    	 email = LimparTexto(SaFileUp.Form("email"))
    end if

    TABELA = "tb_perfil"

    'CAMINHA DO ARQUIVO
	file_path  = "assets/imagem/perfil/"

	if ds_foto = "" then 'INSERIR NOVO REGISTRO'

		if foto = "" then
			response.write "<center>Por favor, indique um arquivo para upload.</center><br>"
			response.end
		else
			set verifica_email = verificaEmail(email, false)

	    	if not verifica_email.EOF Then
				retorno = "{""res"":""error_email"", ""msg"":""Já existe um e-mail cadastrado com esse endereço!""}"    	
	    	else	
				
				foto = LimparTexto(SaFileUp.Form("foto").userFileName)
 				nova_foto = MD5(now) &".jpg"

				'PREPARANDO PARAMETRO PARA INSERIR NO BD
				PARAM = "(nm_email, nm_perfil, dt_nascimento, ds_foto, id_categoria) VALUES ('" & email &"', '" & nm_perfil &"', '" &dt_nascimento &"', '" & nova_foto &"', " & id_categoria &")"
				
				insert_row = insert(TABELA, PARAM, false)

				If insert_row <> "" Then 'se inseriu retorna TRUE
					
					'CRIAR A PASTA COM O ID DO USUÁRIO
					pasta_id = criarPasta(ABSOLUTO_IMAGEM_PERFIL, insert_row)
					
					'#INICIO UPLOAD DE IMAGEM PARA O SERVIDOR#'
					'CAMINHO RELATIVO NO SERVIDOR'
					SaFileUp.Path = Server.MapPath(file_path & insert_row)
				    SaFileUp.SaveAs nova_foto 'SALVA IMAGEM'
				    Set SaFileUp = Nothing 
				    '#FINAL UPLOAD DE IMAGEM PARA O SERVIDOR#'

					IF pasta_id = true Then 'CRIOU A PASTA COM O ID'

						ABSOLUTO_THUMB = ABSOLUTO_IMAGEM_PERFIL & insert_row
						
						pasta_thumb = criarPasta(ABSOLUTO_THUMB, "\thumb\")					

						IF pasta_thumb = false then
							retorno = "{""res"":""error"", ""msg"":""Problema ao criar pasta do THUMB!""}"
						Else
							THUMB = ABSOLUTO_THUMB & "\thumb\"
							call redimensionaImagem(90, 90, ABSOLUTO_IMAGEM_PERFIL & insert_row & "\", THUMB, nova_foto, false)

							'#INICIO ENVIO DE E-MAIL#'
		    				nome_remetente = "KBRTEC"
		    				destino = email
		    				assunto = "Novo Cadastro"
		    				corpo = "Você foi cadastrado com sucesso!"

		    				envio = sendEmail(false,nome_remetente,destino, FALSE, assunto, corpo)
		    				'#INICIO ENVIO DE E-MAIL#'

			    			if envio = false then
			    				retorno = "{""res"":""error"", ""msg"":""Problema ao enviar e-mail!""}"
			    			end if
			    			'FINAL VERIFICA SE ENVIOU E-MAIL'
						end if
						'FINAL VERIFICA SE CRIOU PASTA THUMB'
				    	
					else
						retorno = "{""res"":""error"", ""msg"":""Problema ao criar pasta do perfil!""}"
					End if
					'FINAL VERIFICA SE CRIOU A PASTA'

			    	retorno = "{""res"":""ok"", ""msg"":"""&modulo&" cadastrado com sucesso!"", ""url"": """&url&"""}"	
				else
					retorno = "{""res"":""error"", ""msg"":""Problema ao inserir novo "&modulo&"!""}"
				end if				
				'FINAL VERIFICA SE INSERIU NA TABELA CATEGORIA'
			end if
			'VERIFICA SE NÃO EXISTE E-MAIL IGUAL'
		end If
		'FINAL VERIFICA SE FOTO NÃO ESTÁ VAZIA'

	elseif ds_foto <> "" then
		
		if id_perfil <> "0" then 'SE ID FOR DIFERENTE DE ZERO É UMA ALTERAÇAO

			set verifica_email = verificaEmail(email, id_perfil)

	    	if not verifica_email.EOF Then
				retorno = "{""res"":""error_email"", ""msg"":""Já existe um e-mail cadastrado com esse endereço!""}"    	
	    	else	
				if foto <> "" then 'ALTERAR DADOS E FAZER UM NOVO UPLOAD'
					
					dim fs
					set fs = Server.CreateObject("Scripting.FileSystemObject")

					file = ABSOLUTO_IMAGEM_PERFIL & id_perfil & "\" & ds_foto
					file_thumb = ABSOLUTO_IMAGEM_PERFIL & id_perfil & "\thumb\" & ds_foto

					if fs.FileExists(file) then
					  fs.DeleteFile(file)
					end if

					if fs.FileExists(file_thumb) then
					  fs.DeleteFile(file_thumb)
					end if
					
					set fs=nothing

					foto = LimparTexto(SaFileUp.Form("foto").userFileName)
					nova_foto = MD5(now) &".jpg"

					'CAMINHO RELATIVO NO SERVIDOR'
					SaFileUp.Path = Server.MapPath(file_path & id_perfil)
				    SaFileUp.SaveAs nova_foto 'SALVA IMAGEM'
				    Set SaFileUp = Nothing 

				    'SALVAR O THUMB'
				    ABSOLUTO_THUMB = ABSOLUTO_IMAGEM_PERFIL & id_perfil
				    THUMB = ABSOLUTO_THUMB & "\thumb\"
		    								'altura, largura, caminho, caminho_novo, nome_arquivo, DEBUG
					call redimensionaImagem(90, 90, ABSOLUTO_IMAGEM_PERFIL & id_perfil & "\", THUMB, nova_foto, false)
				end if
				'FINAL VERIFICA SE FOTO É DIFERENTE DE VAZIO'

		    	'INICIO UPDATE
				PARAM = " SET nm_email = '"&email&"'," &" nm_perfil = '"&nm_perfil&"'," & " id_categoria = "&id_categoria & ", dt_nascimento = '"& dt_nascimento&"'"

				if foto <> "" then
					PARAM = PARAM & ", ds_foto = '"&nova_foto&"'"
				end if
				'FINAL VERIFICA SE A FOTO ESTÁ DIFERENTE DE VAZIO'

				WHERE = " WHERE id_perfil = " & id_perfil

							'TABELA, PARAM, WHERE, DEBUG'
				call update(TABELA, PARAM, WHERE, FALSE)
				url = "perfil_form.asp?q=s&acao=alterar&id="&id_perfil
			    retorno = "{""res"":""ok"", ""msg"":"""&modulo&" alterado com sucesso!"", ""url"": """&url&"""}"
	    	end if
	    	'FINAL VERIFICA SE EXISTE E-MAIL CADASTRADO'
		else
			Response.Redirect url	
		end if
		'FINAL VERIFICA SE ID DO PERFIL É DIFERENTE DE 0'
	end if
	'FINAL VERIFICA SE DS_FOTO É DIFERENTE DE VAZIO'
   	Response.write(retorno)

else
	Response.Redirect url
end if
'FINAL VERIFICA METODO DE REQUISIÇÃO IGUAL A POST'

%>