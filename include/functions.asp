<%
'Arquivo criado para criar funções que seram usadas dentro do sistema
'################################################################################################'
'########################### INICIO FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################'
Function validaEmail(ByVal strEmail)
	Dim regEx
	Dim ResultadoHum
	Dim ResultadoDois 
	Dim ResultadoTres
	Set regEx = New RegExp          
	regEx.IgnoreCase = True        
	regEx.Global = True             

	regEx.Pattern	= "[^@\-\.\w]|^[_@\.\-]|[@\.]{2}|(@)[^@]*\1"
	ResultadoHum	= RegEx.Test(strEmail)

	regEx.Pattern	= "@[\w\-]+\."        
	ResultadoDois	= RegEx.Test(strEmail)

	regEx.Pattern	= "\.[a-zA-Z]{2,3}$"  
	ResultadoTres	= RegEx.Test(strEmail)
	Set regEx = Nothing

	If Not (ResultadoHum) And ResultadoDois And ResultadoTres Then
		validaEmail = True
	Else
		validaEmail = False
	End If
End Function


function LimparTexto(str)
	str = trim(str)
	str = lcase(str)
	str = replace(str,"=","")
	str = replace(str,"'","")
	str = replace(str,"""""","")
	str = replace(str," or ","")
	str = replace(str," and ","")
	str = replace(str,"(","")
	str = replace(str,")","")
	str = replace(str,"<","[")
	str = replace(str,">","]")
	str = replace(str,"update","")
	str = replace(str,"-shutdown","")
	str = replace(str,"--","")
	str = replace(str,"'","")
	str = replace(str,"#","")
	str = replace(str,"$","")
	str = replace(str,"%","")
	str = replace(str,"¨","")
	str = replace(str,"&","")
	str = replace(str,"'or'1'='1'","")
	str = replace(str,"--","")
	str = replace(str,"insert","")
	str = replace(str,"drop","")
	str = replace(str,"delet","")
	str = replace(str,"xp_","")
	str = replace(str,"select","")
	str = replace(str,"*","")
	LimparTexto = str
end function

Function get_query(TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG)
	On Error Resume Next
		''INSTACIA O OBJETO ADODB.Connection'
		Set Connect = Server.CreateObject("ADODB.Connection")
		Connect.CursorLocation = 3
		'ABRE CONEXAO COM O  BANCO DE DADOS'
		Connect.Open dB

		'Instancia o objeto do recordset
		Set TEMP = Server.CreateObject("ADODB.Recordset")

		'VERIFICA SE PARAM É FALSE'
		if PARAM = false Then
			PARAM = ""
		end if

		'VERIFICA SE WHERE É FALSE'
		if WHERE = false then
			WHERE = ""
		End if

		'VERIFICA SE INNER É FALSE'
		if INNER = false then
			INNER = ""
		End if

		'VERIFICA SE GROUP_BY É FALSE'
		if GROUP_BY = false then
			GROUP_BY = ""
		End if

		'VERIFICA SE ORDER_BY É FALSE'
		if ORDER_BY = false then
			ORDER_BY = ""
		End if

		'MONTANDO QUERY'
		QUERY = "SELECT * " & PARAM & " FROM " & TABELA & INNER & WHERE & GROUP_BY & ORDER_BY

		'VERIFICA SE DEBUG ESTÁ TRUE'
		IF DEBUG = true then
			Response.write(QUERY)
			Response.End
		end if

		'EXECUTA QUERY'
		TEMP.Open QUERY, Connect, 3, 1
		
		'ENCERRANDO OBJETO ADODB.Recordset'
		Set TEMP.ActiveConnection = Nothing

		'ENCERRANDO OBJETO ADODB.Connection'
		Connect.Close
		Set Connect = Nothing	

		'RECEBENDO E RETORNANDO A CONSULTA'
		Set get_query = TEMP


	On Error Goto 0	
end function

function update(TABELA, PARAM, WHERE, DEBUG)
	On Error Resume Next
		''INSTACIA O OBJETO ADODB.Connection ANTES DE ENTRAR NESSA FUNCTION'
		Set Connect = Server.CreateObject("ADODB.Connection")
		Connect.CursorLocation = 3
		Connect.Open db

		'MONTANDO QUERY'
		QUERY = "UPDATE " & TABELA & PARAM & WHERE

		'VERIFICA SE DEBUG ESTÁ TRUE'
		IF DEBUG = true then
			Response.write(QUERY)
			Response.End
		end if

		'EXECUTA QUERY'
		Connect.execute(QUERY)

		'ENCERRANDO OBJETO ADODB.Connection'
		Connect.Close
		Set Connect = Nothing
	On Error Goto 0	
end function

function delete(TABELA, WHERE, DEBUG)
	On Error Resume Next
		''INSTACIA O OBJETO ADODB.Connection ANTES DE ENTRAR NESSA FUNCTION'
		Set Connect = Server.CreateObject("ADODB.Connection")
		Connect.CursorLocation = 3
		Connect.Open db

		'MONTANDO QUERY'
		QUERY = "DELETE FROM " & TABELA & WHERE

		'VERIFICA SE DEBUG ESTÁ TRUE'
		IF DEBUG = true then
			Response.write(QUERY)
			Response.End
		end if

		'EXECUTA QUERY'
		Connect.execute(QUERY)

		'ENCERRANDO OBJETO ADODB.Connection'
		Connect.Close
		Set Connect = Nothing
	On Error Goto 0	
end function

function insert(TABELA, PARAM, DEBUG)
	On Error Resume Next
		''INSTACIA O OBJETO ADODB.Connection'
		Set Connect = Server.CreateObject("ADODB.Connection")
		Connect.CursorLocation = 3
		Connect.Open db

		'MONTANDO QUERY'
		QUERY = "INSERT INTO "& TABELA & PARAM

		'VERIFICA SE DEBUG ESTÁ TRUE'
		IF DEBUG = true then
			Response.write(QUERY)
			Response.End
		end if

		'EXECUTA QUERY'
		Connect.execute QUERY, RecordsAffected

		codigo_inserido = Connect.Execute( "SELECT @@IDENTITY FROM "&TABELA)(0).Value

	    'ENCERRANDO OBJETO ADODB.Connection'
		Connect.Close
		Set Connect = Nothing

		'RECEBENDO E RETORNANDO A CONSULTA'
	    insert = codigo_inserido

    On Error Goto 0	
end function

Function FormataData(Data, tipo)
	If Data <> "" Then 

		ANO = DatePart("yyyy", Data)
		MES = Right("0" & DatePart("m", Data),2)
		DIA = Right("0" & DatePart("d", Data),2)

		Select Case tipo
			Case 1 'CONVERTE DD/MM/YYYY PARA YYYY/MM/DD'
				FormataData = ANO &  "-" & MES & "-" &  DIA
			Case 2 'CONVERTE YYYY-MM-DD PARA DD/MM/YYYY'
				FormataData = DIA & "/" & MES & "/" & ANO
		End Select
   	end if
   	'FINAL VERIFICA DATA DIFERENTE DE VAZIO'

End Function

Function CriarPasta(ByVal pCaminho, ByVal pNomePasta) 
	On Error Resume Next
		Dim caminho
		Dim pasta 
		Dim objFS
        
        caminho = pCaminho
        pasta = pNomePasta

        Set objFS = Server.CreateObject("Scripting.FileSystemObject")
        
        If Not objFS.FolderExists( caminho & pasta)  Then
        	objFS.CreateFolder( caminho & pasta )
        	criarPasta = true
        	Set objFS = Nothing
        Else
        	criarPasta = false
        	Set objFS = Nothing	
        End If
        
	On Error Goto 0	
End Function

Function sendEmail(remetente,remetente_nome,destino,copia,assunto,corpo)
	
	On Error Resume Next

		Set Mail = Server.CreateObject("Persits.MailSender")

		Mail.Host = "smtp.kbrtec.com.br"
		Mail.Port = 587
		Mail.Username = "weslley.santo@kbrtec.com.br"
		Mail.Password = "wesley2015so"

		'Configura o e-mail do remetente da mensagem que OBRIGATORIAMENTE deve ser um e-mail do seu próprio domínio
		if remetente <> false then
			Mail.From = remetente
		Else
			Mail.From = "no-reply@kbrtec.com.br"
		End if
		'Configura o Nome do remetente da mensagem

		Mail.FromName = remetente_nome

		'Configura os destinatários da mensagem
		Mail.AddAddress destino
		
		IF copia <> false then
			Mail.AddBcc copia
		end if
		
		'Configura o Assunto da mensagem enviada
		Mail.Subject = assunto

		'Configura o conteúdo da Mensagem
		Mail.Body = corpo
		
		Mail.IsHTML = True
		Mail.Send

		If err <> 0 Then
			sendEmail = false
			Set Mail = Nothing
		Else
			sendEmail = true
			Set Mail = Nothing
		End If
				
	On Error Goto 0
End Function

Function renomear(ByVal arquivo, ByVal novoNome)
	Set objFS = Server.CreateObject("Scripting.FileSystemObject")
	extensao = "." & objFS.GetExtensionName(arquivo)
	renomear = novoNome & extensao
	Set objFS = Nothing
End Function

Sub redimensionaImagem(altura, largura, caminho, caminho_novo, nome_arquivo, DEBUG)

	If DEBUG = true then
		Response.write "altura: " & altura
		Response.write "<br/>"
		Response.write "largura: " & largura
		Response.write "<br/>"
		Response.write "caminho: " & caminho
		Response.write "<br/>"
		Response.write "caminho_novo: " & caminho_novo
		Response.write "<br/>"
		Response.write "nome_arquivo: " & nome_arquivo
		Response.End
	end if

	If IsNumeric(largura) And IsNumeric(altura) Then
		Set Jpeg = Server.CreateObject("Persits.Jpeg")
		
		width_img  = largura
		height_img = altura
		
		Jpeg.Open caminho & nome_arquivo
		
		If width_img <> Cint(Jpeg.OriginalWidth) and height_img <> Cint(Jpeg.OriginalHeight) Then 	
			If Cint(Jpeg.OriginalHeight) > height_img Or Cint(Jpeg.OriginalWidth) > width_img Then
				If Cint(Jpeg.OriginalHeight) > Cint(Jpeg.OriginalWidth) Then
					Jpeg.Height  = height_img
					Jpeg.Width   = cint((Jpeg.OriginalWidth * Jpeg.Height)/Jpeg.OriginalHeight)
				Else
					Jpeg.Width   = width_img
					Jpeg.Height  = cint((Jpeg.OriginalHeight * Jpeg.Width)/Jpeg.OriginalWidth)
				End If
			End if
			
			Jpeg.Quality = 100
			'Jpeg.Sharpen 1,150
			'Response.write("caminho_novo: " & caminho_novo)

			Jpeg.Save caminho_novo & nome_arquivo
			
			Set Jpeg = Nothing
		Else
			'Declara a variável a ser usada
			Dim fso

			'Cria a instância com o objeto ObjFile
			Set fso = CreateObject("Scripting.FileSystemObject")

			'COPIA o arquivo da pasta atual para a pasta informada
			fso.CopyFile caminho & nome_arquivo, caminho_novo
			'Move o arquivo da pasta atual para a pasta informada
			'fso.MoveFile caminho & nome_arquivo, caminho_novo & nome_novo

			'Elimina variável da memória 
			Set fso = Nothing
		End If	
			
	End If
End Sub

'################################################################################################'
'########################### FINAL FUNÇÕES PRINCIPAIS DO SISTEMA ################################'
'################################################################################################'

'################################################################################################'
'################################## INICIO FUNÇÕES DO SISTEMA ####################################'
'################################################################################################'

Function get_categorias
	On Error Resume Next

	TABELA = "tb_categoria"
	ORDER_BY = " ORDER BY nm_categoria ASC"
								'TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
   set get_categorias =  get_query(TABELA, false, false, false, false, ORDER_BY, false)
		
	On Error Goto 0	
end function

Function get_perfil
	On Error Resume Next

	TABELA = "tb_perfil p"
	ORDER_BY = " ORDER BY nm_perfil ASC"
	INNER = " LEFT JOIN tb_categoria c ON c.id_categoria = p.id_categoria "
								'TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
   set get_perfil =  get_query(TABELA, false, false, INNER, false, ORDER_BY, FALSE)
		
	On Error Goto 0	
end function

Function desabilitar(param)
	On Error Resume Next

	if param then
		desabilitar = "disabled=""disabled"""
	else
		desabilitar = ""
	end if

		
	On Error Goto 0	
end function

Function verificaCategoria(nm_categoria, id_categoria)
	On Error Resume Next

	TABELA = "tb_categoria"
	WHERE = " WHERE nm_categoria = '"&nm_categoria&"'"
	if id_categoria <> false then
		WHERE = WHERE & " AND id_categoria <> "&id_categoria		
	end if

									'TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
   set verificaCategoria =  get_query(TABELA, false, WHERE, FALSE, false, ORDER_BY, false)
		
	On Error Goto 0	
end function

Function verificaEmail(nm_email, id_perfil)
	On Error Resume Next

	TABELA = "tb_perfil"
	WHERE = " WHERE nm_email = '"&nm_email&"'"
	
	if id_perfil <> false then
		WHERE = WHERE & " AND id_perfil <> "&id_perfil		
	end if

									'TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
   set verificaEmail =  get_query(TABELA, false, WHERE, FALSE, false, ORDER_BY, false)

	On Error Goto 0	
end function

'################################################################################################'
'################################## FINAL FUNÇÕES DO SISTEMA ####################################'
'################################################################################################'
%>