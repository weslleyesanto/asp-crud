<!--#include file="include\config.asp"-->
<%
CATEGORIA = "categoria_list.asp"

If Request.QueryString("q") = "s" Then 
    If Request.QueryString("acao") = "add" Then
        title = "Adicionar Categoria"
        nm_categoria = ""
        id_categoria = "0"
    elseif  Request.QueryString("acao") = "alterar" AND Request.QueryString("id") <> "" then
        
    IF IsNumeric(Request.QueryString("id")) Then 
            title = "Alterar Categoria"

            id_categoria = LimparTexto(Request.QueryString("id"))
            'QUERY VERIFICAR SE CATEGORIA EXISTEM COM ESSE ID'
            TABELA = "tb_categoria "
            WHERE = " WHERE id_categoria = "& id_categoria
                                        'TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
            set retorno_query = get_query(TABELA, false, WHERE, false, false, false, FALSE)

            If not retorno_query.EOF Then 'VERIFICA SE TRUE

                Do While Not retorno_query.EOF
                    id_usuario = retorno_query.Fields("id_categoria").Value
                    nm_categoria = retorno_query.Fields("nm_categoria").Value
                    retorno_query.MoveNext
                Loop
                retorno_query.close
            else 'SE NÃO ENCONTROU VOLTA PRA LISTAGEM'
                Response.Redirect CATEGORIA
            end if
            'FINAL VERIFICA SE RETORNOU ALGO DO SELECT'
        else
            Response.Redirect CATEGORIA    
        end if
        'FINAL VERIFICA SE ID É UM NUMERO'
    else
        Response.Redirect CATEGORIA
    end if
    'FINAL IF TIPO DE AÇÃO'
else
    Response.Redirect CATEGORIA
end if
'FINAL IF Q IGUAL A S' 
%>

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title><%=title%></title>

    </head>

    <body>

        <div id="wrapper">

           <!--#include file="include\menu.asp"-->

           <div id="page-wrapper">

            <div class="container-fluid">

                <h1><%=title%></h1>
        
                <div id="alert" style="display:none;"> </div>

                <form role="form" name="form_post_categoria" method="post" id="form_post_categoria">

                    <label>Nome Categoria</label>
                    <input type="text" required="required" placeholder="Insira uma nova categoria" id="nm_categoria" name="nm_categoria" value="<%=nm_categoria%>" maxlength="50"/>
                    <input type="hidden" id="id_categoria" name="id_categoria" value="<%=id_categoria%>" maxlength="50"/>
                    <button type="submit" class="btn btn-default" name="Salvar" id="btnSalvar">Salvar</button>

                </form>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!--#include file="include\js.asp"-->
    <script src="assets/js/categoria.js"></script>
</body>

</html>
