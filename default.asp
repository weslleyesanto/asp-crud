<!--#include file="include\config.asp"-->
<% 
Set retorno_query = get_perfil 
pasta_thumb = "/thumb/"
%>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Perfil</title>

</head>

<body>

    <div id="wrapper">

       <!--#include file="include\menu.asp"-->

       <div id="page-wrapper">


        <div class="container-fluid">
            <h1>Listagem Perfil</h1>
            <div id="alert" style="display:none;"> </div>
            
            <div id="">
                <a href="perfil_form.asp?q=s&acao=add" title="Adicionar Perfil">Adicionar</a>
                <table id="listaUsuarios">
                    <%  If not retorno_query.EOF Then 'VERIFICA SE TRUE %>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Thumb</th>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th>Data de nascimento</th>
                            <th>Categoria</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                     <%     
                     Do While Not retorno_query.EOF
                            id_perfil = retorno_query.Fields("id_perfil").Value
                            ds_foto = retorno_query.Fields("ds_foto").Value
                            nm_categoria = retorno_query.Fields("nm_categoria").Value
                           %>
                            <tr data-id="<%=id_perfil%>" modulo="perfil" page="default">
                                <td><%=id_perfil%></td>
                                <td>
                        <img src="<%=RELATIVO_IMAGEM_PERFIL&id_perfil&pasta_thumb&ds_foto%>" alt="<%=nm_perfil%>" title="<%=nm_perfil%>" id="caminho_foto" />
                                </td>
                                <td><%=retorno_query.Fields("nm_perfil").Value%></td>
                                <td><%=retorno_query.Fields("nm_email").Value%></td>
                                <td><%=retorno_query.Fields("dt_nascimento").Value%></td>
                                <td>
                                <% 
                                    if isnull(nm_categoria) Then
                                        response.write("Sem categoria")
                                    else
                                        response.write(nm_categoria)
                                    end if
                                %>
                                </td>
                                <td>
                                    <a href="perfil_form.asp?q=s&acao=alterar&id=<%=id_perfil%>" title="Alterar" class="alterar">
                                        Alterar
                                    </a>
                                    <a href="perfil_form.asp?q=s&acao=visualizar&id=<%=id_perfil%>" title="Visualizar">Visualizar</a>
                                    <a href="#" title="Excluir" class="excluir">Excluir</a>
                                    <a href="#" title="Notificar" class="notificar">Notificar</a>
                                </td>
                            </tr>
                            <%  
                            retorno_query.MoveNext
                            Loop
                            retorno_query.close
                            %>
                        </tbody>
                    </table>
                    <% else %>
                    <tr>
                        <td colspan="6">Nenhum registro encontrado!</td>
                    </tr>
                    <%
                    end if
                    %>
                </div>
            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <!--#include file="include\js.asp"-->
    <script src="assets/js/perfil.js"></script>
</body>

</html>
