<!--#include file="include\config.asp"-->
<%

if  Request.ServerVariables("REQUEST_METHOD") = "POST" then
    
    Response.ContentType = "application/json"

    nm_categoria = LimparTexto(Request.Form("nm_categoria"))
    id_categoria = LimparTexto(Request.Form("id_categoria"))
    modulo = "Categoria"
	url = "categoria_list.asp"
	'set verifica_categoria

    if id_categoria <> "0" then 'SE ID FOR DIFERENTE DE ZERO É UMA ALTERAÇAO
		
    	set verifica_categoria = verificaCategoria(nm_categoria, id_categoria)

    	if not verifica_categoria.EOF Then
			retorno = "{""res"":""error"", ""msg"":""Já existe uma "&modulo&" com esse nome!""}"    	
    	else	
	    		'INICIO UPDATE DA CATEGORIA
			TABELA = "tb_categoria"
			PARAM = " SET nm_categoria = '"&nm_categoria&"'"
			WHERE = " WHERE id_categoria = " & id_categoria

						'TABELA, PARAM, WHERE, DEBUG'
			call update(TABELA, PARAM, WHERE, false)

		    retorno = "{""res"":""ok"", ""msg"":"""&modulo&" alterada com sucesso!"", ""url"": """&url&"""}"
    	end if


    else 'SENAO É UMA INSERÇÃO'
    	set verifica_categoria = verificaCategoria(nm_categoria, false)

    	if not verifica_categoria.EOF Then
    		retorno = "{""res"":""error"", ""msg"":""Já existe uma "&modulo&" com esse nome!""}"    	
    	else	
	    	'Salvar no banco de dados na tabela categoria
			TABELA = "tb_categoria"
			PARAM = "(nm_categoria) VALUES ('" & nm_categoria &"')"
			insert_row = insert(TABELA, PARAM, false)

			If insert_row <> "" Then 'se inseriu retorna TRUE
		    	retorno = "{""res"":""ok"", ""msg"":"""&modulo&" cadastrada com sucesso!"", ""url"": """&url&"""}"	
			else
				retorno = "{""res"":""error"", ""msg"":""Problema ao inserir nova categoria!""}"
			end if				
			'FINAL VERIFICA SE INSERIU NA TABELA CATEGORIA'
		end if

    end if
    'FINAL IF VERIFICA SE É INSERT OU UPDATE'
    Response.write(retorno)
else
	Response.Redirect "categoria_list.asp"
end if
'FINAL VERIFICA METODO DE REQUISIÇÃO IGUAL A POST'

%>