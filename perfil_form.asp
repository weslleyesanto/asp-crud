<!--#include file="include\config.asp"-->
<%
REDIRECIONA = "default.asp"

Set categorias = get_categorias
selecionado = ""
visualizar = false
pasta_thumb = "/thumb/"

If Request.QueryString("q") = "s" Then 
    If Request.QueryString("acao") = "add" Then
        title = "Adicionar Perfil"
        nm_perfil = ""
        email = ""
        dt_nascimento = ""
        ds_foto = ""
        id_categoria = ""
        id_perfil = "0"
    elseif  Request.QueryString("acao") = "alterar" AND Request.QueryString("id") <> "" then

        IF IsNumeric(Request.QueryString("id")) Then 

            title = "Alterar Perfil"
            ALTERAR = true
            id_perfil = LimparTexto(Request.QueryString("id"))
            'QUERY VERIFICAR SE CATEGORIA EXISTEM COM ESSE ID'
            TABELA = "tb_perfil p"
            PARAM = ", p.id_categoria as id_categoria_perfil "
            WHERE = " WHERE id_perfil = "& id_perfil
            INNER = " LEFT JOIN tb_categoria c ON c.id_categoria = p.id_categoria "
                                        'TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
            set retorno_query = get_query(TABELA, PARAM, WHERE, INNER, false, false, FALSE)

            If not retorno_query.EOF Then 'VERIFICA SE TRUE

                Do While Not retorno_query.EOF
                    id_perfil = retorno_query.Fields("id_perfil").Value
                    nm_perfil = retorno_query.Fields("nm_perfil").Value
                    email = retorno_query.Fields("nm_email").Value
                    dt_nascimento = FormataData(retorno_query.Fields("dt_nascimento").Value, 2)
                    ds_foto = retorno_query.Fields("ds_foto").Value
                    id_categoria_perfil = retorno_query.Fields("id_categoria_perfil").Value
                    retorno_query.MoveNext
                Loop
                retorno_query.close
            else 'SE NÃO ENCONTROU VOLTA PRA LISTAGEM'
                Response.Redirect REDIRECIONA
            end if
            'FINAL VERIFICA SE RETORNOU ALGO DO SELECT'
        else
            Response.Redirect REDIRECIONA
        end if
        'FINAL VERIFICA SE ID É UM NÚMERO'
    elseif  Request.QueryString("acao") = "visualizar" AND Request.QueryString("id") <> "" then
        IF IsNumeric(Request.QueryString("id")) Then     
            visualizar = true
            title = "Visualizar Perfil"
            id_perfil = LimparTexto(Request.QueryString("id"))

            'QUERY VERIFICAR SE CATEGORIA EXISTEM COM ESSE ID'
            TABELA = "tb_perfil p"
            PARAM = ", p.id_categoria as id_categoria_perfil "
            WHERE = " WHERE id_perfil = "& id_perfil
            INNER = " LEFT JOIN tb_categoria c ON c.id_categoria = p.id_categoria "
            'TABELA, PARAM, WHERE, INNER, GROUP_BY, ORDER_BY, DEBUG
            set retorno_query = get_query(TABELA, PARAM, WHERE, INNER, false, false, FALSE)

            If not retorno_query.EOF Then 'VERIFICA SE TRUE

                Do While Not retorno_query.EOF
                    id_perfil = retorno_query.Fields("id_perfil").Value
                    nm_perfil = retorno_query.Fields("nm_perfil").Value
                    email = retorno_query.Fields("nm_email").Value
                    dt_nascimento = FormataData(retorno_query.Fields("dt_nascimento").Value, 2)
                    ds_foto = retorno_query.Fields("ds_foto").Value
                    id_categoria_perfil = retorno_query.Fields("id_categoria_perfil").Value
                    nm_categoria_perfil = retorno_query.Fields("nm_categoria").Value
                    retorno_query.MoveNext
                Loop
                retorno_query.close
            else 'SE NÃO ENCONTROU VOLTA PRA LISTAGEM'
                Response.Redirect REDIRECIONA
            end if
            'FINAL VERIFICA SE RETORNOU ALGO DO SELECT'
        else
            Response.Redirect REDIRECIONA
        end if
        'FINAL VERIFICA SE ID É UM NÚMERO'
    else
        Response.Redirect REDIRECIONA
    end if
    'FINAL IF TIPO DE AÇÃO'
else
    Response.Redirect REDIRECIONA
end if
'FINAL IF Q IGUAL A S' 
%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><%=title%></title>

</head>

<body>

    <div id="wrapper">

     <!--#include file="include\menu.asp"-->

     <div id="page-wrapper">

        <div class="container-fluid">

            <h1><%=title%></h1>

            <div id="alert" style="display:none;"> </div>

            <form role="form" name="form_post_perfil" method="post" id="form_post_perfil" enctype="multipart/form-data">
                <% if visualizar = false then%>
                <label>Nome</label>
                <input type="text" required="required" placeholder="Nome" id="nm_perfil" name="nm_perfil" value="<%=nm_perfil%>"  maxlength="100"/>
                <br/><br/>
                <label>E-mail</label>
                <input type="email" autofocus maxlength="100" required="required" placeholder="seu@email.com" id="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" value="<%=email%>" />
                <br/><br/>
                <label>Data de nascimento</label>
                <input type="text" required="required" placeholder="DD/MM/YYYY" id="dt_nascimento" name="dt_nascimento" value="<%=dt_nascimento%>"  maxlength="10"/>
                <br/><br/>
                <label>Foto</label>
                <input type="file" id="foto" name="foto" ds-foto="<%=ds_foto%>" />

                <% IF ALTERAR = TRUE Then %>
                <br/><br/>
                <img src="<%=RELATIVO_IMAGEM_PERFIL&id_perfil&pasta_thumb&ds_foto%>" alt="<%=nm_perfil%>" title="<%=nm_perfil%>" id="caminho_foto" />
                <% ENd if%>
                <br/><br/>
                <label>Categoria</label>
                <select name="id_categoria" id="id_categoria" >
                    <option value="">Sem categoria</option>
                    <%  
                            If not categorias.EOF Then 'VERIFICA SE TRUE 
                            Do While Not categorias.EOF

                            pk_categoria = categorias.Fields("id_categoria").Value
                            if pk_categoria = id_categoria_perfil then 
                            selecionado = "selected=""selected"""
                            else
                            selecionado = ""
                            end if
                                'FINAL VERICIA SE ID SELECIONADO É IGUAL ALGUM DO BANCO'
                                %>
                                <option value="<%=pk_categoria%>" <%=selecionado%>>
                                    <%=categorias.Fields("nm_categoria").Value%>
                                </option>
                                <%
                                categorias.MoveNext
                                Loop
                                'FINAL LOOP WHILE'
                                categorias.close
                                end If
                            'FINAL _GET CATEGORIAS'
                            %>
                        </select>
                        <br/><br/>
                        <input type="hidden" id="id_perfil" name="id_perfil" value="<%=id_perfil%>" />
                        <input type="submit" name="Salvar" id="btnSalvar" value="Salvar"/>
                        <% else %>
                            <label>Nome: <%=nm_perfil%></label>
                            <br/><br/>
                            <label>E-mail: <%=email%></label>
                            <br/><br/>
                            <label>Data de nascimento : <%=dt_nascimento%></label>
                            <br/><br/>
                            <label>Foto: <img src="<%=RELATIVO_IMAGEM_PERFIL&id_perfil&pasta_thumb&ds_foto%>" alt="<%=nm_perfil%>" title="<%=nm_perfil%>" id="caminho_foto" /></label>
                            <br/><br/>
                            <label>Categoria: <%=nm_categoria_perfil%></label>
                        <% end if%>

                    </form>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- /#page-wrapper -->

        </div>
        <!-- /#wrapper -->
        <!--#include file="include\js.asp"-->
        <script src="assets/js/perfil.js"></script>
    </body>

    </html>
