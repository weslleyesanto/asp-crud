<!--#include file="include\config.asp"-->
<% Set retorno_query = get_categorias %>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Categorias</title>

</head>

<body>

    <div id="wrapper">

     <!--#include file="include\menu.asp"-->

     <div id="page-wrapper">

        <div class="container-fluid">

            <h1>Listagem Categorias</h1>

            <a href="categoria_form.asp?q=s&acao=add" title="Adicionar Categoria">
                Adicionar
            </a>

            <div id="alert" style="display:none;"> </div>
            <table id="listaCategoria">
             <%  If not retorno_query.EOF Then 'VERIFICA SE TRUE %>
             <thead>
                <tr>
                    <th>#</th>
                    <th>Nome</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
               <%
               Do While Not retorno_query.EOF
               %>
               <tr data-id="<%=retorno_query.Fields("id_categoria").Value%>" modulo="categoria" page="categoria_list">
                <td><%=retorno_query.Fields("id_categoria").Value%></td>
                <td><%=retorno_query.Fields("nm_categoria").Value%></td>
                <td>
                    <a href="categoria_form.asp?q=s&acao=alterar&id=<%=retorno_query.Fields("id_categoria").Value%>" title="Alterar" class="alterar">Alterar</a>
                    <a href="#" title="Excluir" class="excluir">Excluir</a>
                </td>
            </tr>
            <%  
            retorno_query.MoveNext
            Loop
            retorno_query.close
            %>
        </tbody>
        <%
        else
        %>
        <tr>
            <td colspan="6">Nenhum registro encontrado!</td>
        </tr>
        <%
        end if
        %>
    </table>

</div>
<!-- /.container-fluid -->

</div>
<!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<!--#include file="include\js.asp"-->
<script src="assets/js/categoria.js"></script>
</body>

</html>
