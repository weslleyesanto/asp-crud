var perfil = (function() {
	var P = {};
	var files;

	P.setDHTML = function() {
		
		$("#dt_nascimento").mask("99/99/9999");
		
		$('#form_post_perfil').submit( function(e){
			e.preventDefault();
		});

		$('input[type=file]').on('change', function(event){
			files = event.target.files;
		});

		$('#btnSalvar').on('click', function() {
			P.salvar();
		});

		$('.notificar').on('click', function(){
			var id_perfil = $(this).parents('tr').attr('data-id');
			P.notificar(id_perfil);

		});

	};

	P.salvar = function(event) {

		var nm_perfil = $('#nm_perfil').val();
		var email = $('#email').val();
		var dt_nascimento = $('#dt_nascimento').val();
		var foto = $('#foto').attr('ds-foto');
		var value_foto = $('#foto').val();
		var caminho_foto = $('#caminho_foto').attr('src');
		var id_categoria = $('#id_categoria').val();
		var id_perfil = $('#id_perfil').val();
		
		var error = false;

		if (nm_perfil == '') {
			msg = "Preencha o nome!";
			P.alertError(msg);
			error = error ? error : true;
		}else if (email == '') {
			msg = "Preencha o E-mail!";
			P.alertError(msg);
			error = error ? error : true;
		}else if(!P.validateEmail(email)){
			msg = "Preencha o E-mail corretamente!";
			P.alertError(msg);
			error = error ? error : true;
		}else if (dt_nascimento == '') {
			msg = "Preencha a data de nascimento!";
			P.alertError(msg);
			error = error ? error : true;
		}else if ((value_foto == '' || typeof value_foto == 'undefined') 
			&& (caminho_foto == '' || typeof caminho_foto == 'undefined')) {
			msg = "Inclua a foto!";
			P.alertError(msg);
			error = error ? error : true;
		}else if (id_categoria == '') {
			msg = "Selecione uma categoria!";
			P.alertError(msg);
			error = error ? error : true;
		}

		if(!error){
			
			var data = new FormData();

			if(typeof files != 'undefined'){
				$.each(files, function(key, value)
				{
					data.append('foto', value);
				});
			}

			data.append('nm_perfil', nm_perfil);
			data.append('ds_foto', foto);
			data.append('email', email);
			data.append('dt_nascimento', dt_nascimento);
			data.append('id_categoria', id_categoria);
			data.append('id_perfil', id_perfil);

			$.ajax({
				url: "perfil_post.asp", // Url to which the request is send
				type: "POST",             // Type of request to be send, called as method
				data: data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
				contentType: false,       // The content type used when sending data to the server.
				cache: false,             // To unable request pages to be cached
				dataType: 'json',
				processData:false,        // To send DOMDocument or non processed data file it is set to false
				success: function(data)   // A function to be called if request succeeds
				{
					if(data.res == 'error_email'){
						P.alertError(data.msg);
						return false 
					}else if(data.res == 'ok'){
						alert(data.msg); 
						window.location.href = data.url;
						return false;	
					}else if(data.res == 'error'){
						P.alertError(data.msg);
						return false 
					}
				},
				error: function(data){
					P.alertError(data.msg);
					return false;
				}
			});
		}

	}

	P.notificar = function(id_perfil){

		var data = {
			id_perfil: id_perfil
		};

		$.post('notificar.asp', data, function(ret){
			if(ret.res == "ok"){
				alert(ret.msg); 
				//window.location.href = ret.url;
				return false;	
			}else if(ret.res == "error"){
				P.alertError(ret.msg);
				return false
			}
		}, 'json');


		return false;
	}

	P.alertError = function(msg){
		$('#alert')
		.addClass('alert-danger')
		.css({'display':'block'})
		.html('<strong style="color:red">Error!</strong> '+msg);
	}

	P.validateEmail = function (email)
	{
		//er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
		er = /[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/
		if(er.exec(email)){
			return true;
		}else{
			return false;
		}
	};

	$(function() {
		P.setDHTML();
	});

	return P;
})();