var script = (function() {
	var S = {}, winH, winW;

	S.setDHTML = function() {

		$('.excluir').on('click', function(){
			var modulo = $(this).parents('tr').attr('modulo');
			var page = $(this).parents('tr').attr('page');
			var confirmar = confirm("Deseja realmente exluir esse(a) "+modulo+"?");

			if (confirmar) {
				var id = $(this).parents('tr').attr('data-id');
				S.excluir(modulo, id, page);
				return false;
			} 			

		});

	};


	S.excluir = function(modulo, id, page){

		var data = {
			modulo: modulo,
			page: page,
			id: id
		};

		$.post('excluir.asp', data, function(ret){

			if(ret.res == "ok"){
				$('#alert')
				.css({'display':'block'})
				.html('<strong style="color: green">OK!</strong> '+ret.msg);
				setTimeout(function(){ window.location.href = ret.url; }, 2000);
				return false;	
			}else if(ret.res == "error"){
				$('#alert')
				.addClass('alert-danger')
				.css({'display':'block'})
				.html('<strong style="color: red">Error!</strong> '+ret.msg);
				return false;
			}
		}, 'json');


		return false;
	}

	S.alertError = function(msg){
		$('#alert')
			.addClass('alert-danger')
			.css({'display':'block'})
			.html('<strong style="color:red">Error!</strong> '+msg);
	}


	$(function() {
		S.setDHTML();
	});

	return S;
})();